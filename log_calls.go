package log

// Start is used for the entry into a function.
func Start(context interface{}) {
	Up1.Start(context, "")
}

// Startf is used for the entry into a function with a formatted message.
func Startf(context interface{}, format string, a ...interface{}) {
	Up1.Startf(context, "", format, a...)
}

// Complete is used for the exit of a function.
func Complete(context interface{}) {
	Up1.Complete(context, "")
}

// Completef is used for the exit of a function with a formatted message.
func Completef(context interface{}, format string, a ...interface{}) {
	Up1.Completef(context, "", format, a...)
}

// CompleteErr is used to write an error with complete into the trace.
func CompleteErr(err error, context interface{}) {
	Up1.CompleteErr(err, context, "")
}

// CompleteErrf is used to write an error with complete into the trace with a formatted message.
func CompleteErrf(err error, context interface{}, format string, a ...interface{}) {
	Up1.CompleteErrf(err, context, "", format, a...)
}

// Err is used to write an error into the trace.
func Err(err error, context interface{}) {
	Up1.Err(err, context, "")
}

// Errf is used to write an error into the trace with a formatted message.
func Errf(err error, context interface{}, format string, a ...interface{}) {
	Up1.Errf(err, context, "", format, a...)
}

// ErrFatal is used to write an error into the trace then terminate the program.
func ErrFatal(err error, context interface{}) {
	Up1.ErrFatal(err, context, "")
}

// ErrFatalf is used to write an error into the trace with a formatted message then terminate the program.
func ErrFatalf(err error, context interface{}, format string, a ...interface{}) {
	Up1.ErrFatalf(err, context, "", format, a...)
}

// Infof is used to write information into the trace with a formatted message.
func Infof(context interface{}, format string, a ...interface{}) {
	Up1.Infof(context, "", format, a...)
}

// Info is used to write information.
func Info(context interface{}, msg string) {
	Up1.Info(context, "", msg)
}

// Warnf is used to write a warning into the trace with a formatted message.
func Warnf(context interface{}, format string, a ...interface{}) {
	Up1.Warnf(context, "", format, a...)
}

// Queryf is used to write a query into the trace with a formatted message.
func Queryf(context interface{}, format string, a ...interface{}) {
	Up1.Queryf(context, "", format, a...)
}

// DataKV is used to write a key/value pair into the trace.
func DataKV(context interface{}, key string, value interface{}) {
	Up1.DataKV(context, "", key, value)
}

// DataBlock is used to write a block of data into the trace.
func DataBlock(context interface{}, key string, block interface{}) {
	Up1.DataBlock(context, "", key, block)
}

// DataString is used to write a string with CRLF each on their own line.
func DataString(context interface{}, message string) {
	Up1.DataString(context, "", message)
}

// DataTrace is used to write a block of data from an io.Stringer respecting each line.
func DataTrace(context interface{}, formatters ...Formatter) {
	Up1.DataTrace(context, "", formatters...)
}
