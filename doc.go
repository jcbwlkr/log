// Package log is an important part of the application and having a consistent
// logging mechanism and structure is mandatory. With several teams writing
// different components that talk to each other, being able to read each others
// logs could be the difference between finding bugs quickly or wasting hours.
//
// Loggers
//
// With the log package in the standard library, we have the ability to create
// custom loggers that can be configured to write to one or many devices. Since
// we use syslog to send logging output to a central log repository, our logger
// can be configured to just write to stdout. This not only simplifies things
// for us, but will keep each log trace in correct sequence.
//
// Logging levels
//
// This package does not included logging levels. Everything needs to be logged
// to help trace the code and find bugs. There is no such thing as over logging.
// By the time you decide to change the logging level, it is always too late.
// The question of performance comes up quite a bit. If the only performance
// issue we see is coming from logging, we are doing very well. I have had these
// opinions for a long time, but if you want more clarity on the subject listen
// to this recent podcast:
//
// Jon Gifford On Logging And Logging Infrastructure:
//
//   http://www.se-radio.net/2015/02/episode-220-jon-gifford-on-logging-and-logging-infrastructure
//
// Robert Blumen talks to Jon Gifford of Loggly about logging and logging
// infrastructure. Topics include logging defined, purposes of logging, uses of
// logging in understanding the run-time behavior of programs, who produces logs,
// who consumes logs and for what reasons, software as the consumer of logs,
// log formats (structured versus free form), log meta-data, logging APIs,
// logging as coding, logging and frameworks, the massive hairball of log file
// management, modern logging infrastructure in which log records are stored and
// indexed in a search engine, how searchable logs have transformed the uses of
// log data, log data and analytics, leveraging the log database for statistical
// insights, performance and resource issues of logging, are logs really different
// than other data that systems record in databases, and how log visualization gives
// users insights into their system. The show wraps up with a discussion of open
// source logging platforms versus commercial SAAS providers.
//
// Tracing Formats
//
// There are two types of tracing lines we need to log. One is a trace line that
// describes where the program is, what it is doing and any data associated with
// that trace. The second is formatted data such as a JSON document or binary
// dump of data. Each serve a different purpose but they both exists within the
// same scope of space and time.
//
// The format of each trace line needs to be consistent and helpful or else the
// logging will just be noise and ultimately useless.
//
//     PREFIX[PID]: YYYY/MM/DD HH:MM:SS.ZZZ: file.go:LN: Context: Func: Tag: Var[value]: Message
//
// Here is a breakdown of each section and a sample value:
//
//     PREFIX           The application or service name. Set on Init.
//                      Ex. CHADWICK
//
//     [PID]:           The process id for the running program.
//
//     YYYY/MM/DD       Date of the trace log line in UTC.
//                      Ex. 2015/03/23
//
//     HH:MM:SS.ZZZZZZ  Time of the trace log line with microsecond in UTC.
//                      Ex. 14:02:42.123
//
//     file.go:LN:      The name of the source code file and line number.
//                      Ex. main.go:15:
//
//     Context:         This could be a userid, session or something that
//                      maintains a context across multiple trace lines.
//                      There is no way to get a goroutine id
//
//     Func:            The name of the function writing the trace log.
//                      Ex. GetResponse or context.GetSession
//
//     Tag:             The tag of trace line.
//       Started:           Start of a function/method call.
//       Completed:         Return of a function/method call.
//       Completed ERROR:   Return of a function/method call with error.
//       ERROR:             Error trace.
//       Warning:           Warning trace.
//       Info:              General information.
//       DATA:              A dump of data
//       Query:             Any query that can be copied/pasted and run.
//
//     Var[value]:      Optional, Data values, parameters or return values.
//                      Ex. ID[1234]
//
//     Messages:        Optional, Any extended information with proper grammar.
//                      Ex. Waiting on SMSC to acknowledge request.
//
// Here are examples of how trace lines would show in the log:
//
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: rule.go:82: 5567: Get: Started: ID[1234]
//     CHADWICK[69910]: 2015/03/23 14:02:42.423: rule.go:85: 5567: Get: Query: sp_Get 1234
//     CHADWICK[69910]: 2015/03/23 14:02:42.523: rule.go:90: 5567: Get: Completed: Found[1]
//
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: rule.go:82: 5567: Get: Started: ID[1234]
//     CHADWICK[69910]: 2015/03/23 14:02:42.423: rule.go:85: 5567: Get: Query: sp_Get 1234
//     CHADWICK[69910]: 2015/03/23 14:02:42.523: rule.go:90: 5567: Get: Completed: ERROR: Not Found
//
// In the end, we want to see the flow of most functions starting and completing
// so we can follow the code in the logs. We want to quickly see and filter errors,
// which can be accomplished by using a capitalized version of the word ERROR.
//
// The context is an important value. The context allows us to extract trace lines
// for one context over others. Maybe in this case 8890 represents a user id.
//
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: rule.go:82: 5567: Get: Started: ID[1234]     <- 5567
//     CHADWICK[69910]: 2015/03/23 14:02:42.423: rule.go:85: 5567: Get: Query: sp_Get 1234    <- 5567
//     CHADWICK[69910]: 2015/03/23 14:02:43.235: rule.go:82: 8890: Get: Started: ID[4123]     <-   8890
//     CHADWICK[69910]: 2015/03/23 14:02:43.523: rule.go:90: 5567: Get: Completed: Found[1]   <- 5567
//     CHADWICK[69910]: 2015/03/23 14:02:43.626: rule.go:85: 8890: Get: Query: sp_Get 4123    <-   8890
//     CHADWICK[69910]: 2015/03/23 14:02:44.694: rule.go:90: 8890: Get: Completed: Found[1]   <-   8890
//
// When there is a need to dump formatted data into the logs, there are three
// approaches. If the data can be represented as key/value pairs, you can write
// each pair on their own line with the DATA tag:
//
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Protocol Discriminator: 17
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: DSMCC Type:             2
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Message ID:             0x4021 (ClientReleaseConfirm)
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Transaction ID:         0x00000003
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Reserved:               0xFF
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Adaptation Length:      0 bytes
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Message Length:         16 = 0x0010
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Message Bytes:          (0x0000) EE 6E 11 00 00 00 3E EA DE 18 00 00 00 00 00 00
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Session ID
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Device ID:              ee6e11000000
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Session Number:         0x3EEADE18
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Response:               0x0000 = 0
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: UU Data Count:          0 bytes
//     CHADWICK[69910]: 2015/03/23 14:02:42.123: dsmcc.go:123: 5567: DsmccDump: DATA: Private Data Count:     0 bytes
//
// When there is a single block of data to dump, then it can be written as a
// single multi-line trace:
//
//     CHADWICK: 2015/03/23 14:02:42.123: cgwp.go:567: 5567: Teardown: DATA: XML
//     CHADWICK: 2015/03/23 14:02:42.123: cgwp.go:567: 5567: Teardown: DATA: <?xml version="1.0" encoding="utf-8"?>
//     CHADWICK: 2015/03/23 14:02:42.123: cgwp.go:567: 5567: Teardown: DATA: <TeardownSession
//     CHADWICK: 2015/03/23 14:02:42.123: cgwp.go:567: 5567: Teardown: DATA: xmlns="urn:com:comcast:ngsrm:s1:2010:12:03"
//     CHADWICK: 2015/03/23 14:02:42.123: cgwp.go:567: 5567: Teardown: DATA: onDemandSessionId="7591a2ebfa574133bce831163356ccb2"
//     CHADWICK: 2015/03/23 14:02:42.123: cgwp.go:567: 5567: Teardown: DATA: reasonCode="200"
//     CHADWICK: 2015/03/23 14:02:42.123: cgwp.go:567: 5567: Teardown: DATA: clientReasonCode="0x1"
//     CHADWICK: 2015/03/23 14:02:42.123: cgwp.go:567: 5567: Teardown: DATA: >
//     CHADWICK: 2015/03/23 14:02:42.123: cgwp.go:567: 5567: Teardown: DATA: </TeardownSession>
//
// When special block formatting required, the Stringer interface can be
// implemented to format data in custom ways:
//   CHADWICK[69910]: 2009/11/10 20:00:00.000: dsmcc.go:155: 5567: Data_String: Started:
//   CHADWICK[69910]: 2009/11/10 20:00:00.000: dsmcc.go:162: 5567: Data_String: DATA: Message Bytes:  (0x0000) EE 6E 11 00 00 00 3E EA DE 18 00 00 2D 00 00 00
//   CHADWICK[69910]: 2009/11/10 20:00:00.000: dsmcc.go:162: 5567: Data_String: DATA:                 (0x0010) 3E EA DE
//   CHADWICK[69910]: 2009/11/10 20:00:00.000: dsmcc.go:164: 5567: Data_String: Completed:
//
// API Documentation and Examples
//
// The API for the log package is focused on initializing the logger and then
// provides function abstractions for the different tags we have defined.
//
package log
