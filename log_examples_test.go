package log

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strconv"
	"testing"
)

// ExampleStart provides a basic example for using the log package.
func ExampleStart() {
	// Init the log system using a buffer for testing.
	buf := new(bytes.Buffer)
	InitTest("EXAMPLE", buf)

	{
		Start("1234", "Basic")

		v, err := strconv.ParseInt("10", 10, 64)
		if err != nil {
			CompleteErr(err, "1234", "Basic")
			return
		}

		Completef("1234", "Basic", "Conv[%d]", v)
	}

	// Flush the output for testing.
	fmt.Println(buf.String())
	// Output:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Basic: Started:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Basic: Completed: Conv[10]
}

// ExampleErr provides an example of logging an error.
func ExampleErr() {
	// Init the log system using a buffer for testing.
	buf := new(bytes.Buffer)
	InitTest("EXAMPLE", buf)

	{
		Start("1234", "Error")

		v, err := strconv.ParseInt("1080980980980980980898908", 10, 64)
		if err != nil {
			CompleteErr(err, "1234", "Error")

			// Flush the output for testing.
			fmt.Println(buf.String())
			return
		}

		Completef("1234", "Error", "Conv[%d]", v)
	}
	// Output:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Error: Started:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Error: Completed ERROR: strconv.ParseInt: parsing "1080980980980980980898908": value out of range
}

// ExampleDataKV provides an example of logging K/V pair data.
func ExampleDataKV() {
	// Init the log system using a buffer for testing.
	buf := new(bytes.Buffer)
	InitTest("EXAMPLE", buf)

	{
		Start("1234", "Data_KV")

		DataKV("1234", "Data_KV", "Protocol Discriminator", 17)
		DataKV("1234", "Data_KV", "Transaction ID", 0x00000003)

		Complete("1234", "Data_KV")
	}

	// Flush the output for testing.
	fmt.Println(buf.String())
	// Output:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_KV: Started:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_KV: DATA: Protocol Discriminator: 17
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_KV: DATA: Transaction ID: 3
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_KV: Completed:
}

// ExampleDataBlock provides an example of logging a block of data.
func ExampleDataBlock() {
	// Init the log system using a buffer for testing.
	buf := new(bytes.Buffer)
	InitTest("EXAMPLE", buf)

	{
		Start("1234", "Data_Block")

		data := `<?xml version="1.0" encoding="utf-8"?>
<TeardownSession
xmlns="urn:com:comcast:ngsrm:s1:2010:12:03"
onDemandSessionId="7591a2ebfa574133bce831163356ccb2"
reasonCode="200"
clientReasonCode="0x1"
>
</TeardownSession>`

		DataBlock("1234", "Data_Block", "XML", data)

		Complete("1234", "Data_Block")
	}

	// Flush the output for testing.
	fmt.Println(buf.String())
	// Output:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_Block: Started:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_Block: DATA: <?xml version="1.0" encoding="utf-8"?>
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_Block: DATA: <TeardownSession
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_Block: DATA: xmlns="urn:com:comcast:ngsrm:s1:2010:12:03"
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_Block: DATA: onDemandSessionId="7591a2ebfa574133bce831163356ccb2"
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_Block: DATA: reasonCode="200"
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_Block: DATA: clientReasonCode="0x1"
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_Block: DATA: >
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_Block: DATA: </TeardownSession>
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_Block: Completed:
}

type Message []byte

// Format implements the Formatter interface to produce logging output
// for this slice of bytes.
func (m Message) Format() string {
	var buf bytes.Buffer

	// Message Bytes:	(0x0000) EE 6E 11 00 00 00 3E EA DE 18 00 00 2D 00 00 00
	// 					(0x0010) 00 00 00 00 00 0A 16 C3 9B 00 00 00 00 00 00 00

	fmt.Fprintf(&buf, "Message Bytes:\t")

	rows := (len(m) / 16) + 1
	l := len(m)
	for row := 0; row < rows; row++ {
		var r []byte
		st := row * 16

		if row < (rows - 1) {
			r = m[st : st+16]
		} else {
			r = m[st : st+(l-st)]
		}

		if row > 0 {
			fmt.Fprintf(&buf, "\t\t\t")
		}

		fmt.Fprintf(&buf, "(0x%.4X)", st)

		for i := 0; i < len(r); i++ {
			fmt.Fprintf(&buf, " %.2X", r[i])
		}
		fmt.Fprintf(&buf, "\n")
	}

	return buf.String()
}

// ExampleDataTrace provides an example of logging from a fmt.Stringer.
func ExampleDataTrace() {
	/*
		// Message is a named type (type Message []byte) and implements the Stringer interface.
		type Message []byte

		// The Formatter interface is implemented for the named type.
		func (m Message) Format() string {
			var buf bytes.Buffer

			// Message Bytes:	(0x0000) EE 6E 11 00 00 00 3E EA DE 18 00 00 2D 00 00 00
			// 					(0x0010) 00 00 00 00 00 0A 16 C3 9B 00 00 00 00 00 00 00

			fmt.Fprintf(&buf, "Message Bytes:\t")

			rows := (len(m) / 16) + 1
			l := len(m)
			for row := 0; row < rows; row++ {
				var r []byte
				st := row * 16

				if row < (rows - 1) {
					r = m[st : st+16]
				} else {
					r = m[st : st+(l-st)]
				}

				if row > 0 {
					fmt.Fprintf(&buf, "\t\t\t")
				}

				fmt.Fprintf(&buf, "(0x%.4X)", st)

				for i := 0; i < len(r); i++ {
					fmt.Fprintf(&buf, " %.2X", r[i])
				}
				fmt.Fprintf(&buf, "\n")
			}

			return buf.String()
		}
	*/

	// Init the log system using a buffer for testing.
	buf := new(bytes.Buffer)
	InitTest("EXAMPLE", buf)

	{
		Start("1234", "Data_String")

		b1 := []byte{0xEE, 0x6E, 0x11, 0x00, 0x00, 0x00, 0x3E, 0xEA, 0xDE, 0x18, 0x00, 0x00, 0x2D, 0x00, 0x00, 0x00, 0x3E, 0xEA, 0xDE}
		b2 := []byte{0xEE, 0x6E, 0x11, 0x00, 0x00, 0x00, 0x3E, 0xEA, 0xDE, 0x18, 0x00, 0x00, 0x2D, 0x00, 0x00, 0x00, 0x3E, 0xEA, 0xDE}

		DataTrace("1234", "Data_String", Message(b1), Message(b2))

		Complete("1234", "Data_String")
	}

	// Flush the output for testing.
	fmt.Println(buf.String())
	// Output:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_String: Started:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_String: DATA: Message Bytes:	(0x0000) EE 6E 11 00 00 00 3E EA DE 18 00 00 2D 00 00 00
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_String: DATA: 			(0x0010) 3E EA DE
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_String: DATA: Message Bytes:	(0x0000) EE 6E 11 00 00 00 3E EA DE 18 00 00 2D 00 00 00
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_String: DATA: 			(0x0010) 3E EA DE
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Data_String: Completed:
}

// ExampleTracef provides an example of logging from a fmt.Stringer and also tests newline handling.
func ExampleTracef() {
	// Init the log system using a buffer for testing.
	buf := new(bytes.Buffer)
	InitTest("EXAMPLE", buf)

	{
		// Messages without a newline should have one added, and message
		// that have a newline should *not* have an extra one added.
		Tracef("1234", "Tracef", "%s: %s", "1234", "Basic")
		Tracef("1234", "Tracef", "%s: %s\n", "1234", "Basic")
		Tracef("1234", "Tracef", "%s: %s", "ABCD", "Basic")
		Tracef("1234", "Tracef", "%s: %s\n", "ABCD", "Basic")
	}

	// Flush the output for testing.
	fmt.Println(buf.String())
	// Output:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Tracef: Info: 1234: Basic
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Tracef: Info: 1234: Basic
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Tracef: Info: ABCD: Basic
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Tracef: Info: ABCD: Basic
}

// ExampleUplevel_Start provides an example of using the level up functionality.
// For example, the following code would generate a log message with line
// number 13 (the line number where handleErr is called) and function name
// "example.someFunc".
//
//    1: package example
//    2:
//    3: func handleErr(err error, context interface{}, function string) {
//    4:        ...
//    5:        // log with caller's file, line, and function name.
//    6:        log.Up1.Err(err, context, function)
//    7:        ...
//    8: }
//    9:
//   10: func someFunc() {
//   11:        ...
//   12:        if err := doSomething(); err != nil {
//   13:                handleErr(err, context, "")
//   14:        }
//   15:        ...
//   16: }
func ExampleUplevel_Start() {
	// Init the log system using a buffer for testing.
	buf := new(bytes.Buffer)
	InitTest("EXAMPLE", buf)

	{
		levelUp := func(context interface{}, function string) {
			Up1.Tracef(context, function, "Test")
		}

		Start("1234", "Basic")

		levelUp("1234", "Basic")

		Complete("1234", "Basic")
	}

	// Flush the output for testing.
	fmt.Println(buf.String())
	// Output:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Basic: Started:
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Basic: Info: Test
	// EXAMPLE[69910]: 2009/11/10 15:00:00.000: file.go:512: 1234: Basic: Completed:
}

func BenchmarkTracef(b *testing.B) {
	InitTest("BENCHMARK", ioutil.Discard)
	for i := 0; i < b.N; i++ {
		Tracef("context", "function", "This is a test %d this is a test %d this is a test %d", i, i, i)
	}
}
